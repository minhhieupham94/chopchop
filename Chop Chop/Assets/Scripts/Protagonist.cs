using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Protagonist : MonoBehaviour
{
    public InputReader inputReader;

    private void OnEnable()
    {
		inputReader.attackEvent += OnAttack;
		inputReader.extraEvent += OnExtraAction;
		inputReader.interactEvent += OnInteract;
		inputReader.jumpEvent += OnJump;
		inputReader.pauseEvent += OnPause;
		inputReader.moveEvent += OnMove;
	}

    private void OnDisable()
    {
		inputReader.attackEvent -= OnAttack;
		inputReader.extraEvent -= OnExtraAction;
		inputReader.interactEvent -= OnInteract;
		inputReader.jumpEvent -= OnJump;
		inputReader.pauseEvent -= OnPause;
		inputReader.moveEvent -= OnMove;
	}

    public void OnAttack()
	{
		Debug.Log("Attack");
	}

	public void OnExtraAction()
	{
		Debug.Log("ExtraAction");
	}

	public void OnInteract()
	{
		Debug.Log("Interact");
	}

	public void OnJump()
	{
		Debug.Log("Jump");
	}

	public void OnMove(Vector2 vector2)
	{
		Debug.Log("Move " + vector2);
	}

	public void OnPause()
	{
		Debug.Log("Pause");
	}
}
