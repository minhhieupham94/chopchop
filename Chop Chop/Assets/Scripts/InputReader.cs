using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputReader : MonoBehaviour, GameInput.IGamePlayActions
{
	public Action attackEvent;
	public Action extraEvent;
	public Action interactEvent;
	public Action jumpEvent;
	public Action pauseEvent;
	public Action<Vector2> moveEvent;

	GameInput gameInput;

	private void OnEnable()
	{
		if (gameInput == null)
		{
			gameInput = new GameInput();
			gameInput.@GamePlay.SetCallbacks(this);
		}
		gameInput.@GamePlay.Enable();

		Debug.Log("I'm enabled");
	}

	private void OnDisable()
	{
		gameInput.@GamePlay.Disable();
	}

	public void OnAttack(InputAction.CallbackContext context)
	{
		if (context.phase == InputActionPhase.Started)
			attackEvent?.Invoke();
	}

	public void OnExtraAction(InputAction.CallbackContext context)
	{
		if (context.phase == InputActionPhase.Started)
			extraEvent?.Invoke();
	}

	public void OnInteract(InputAction.CallbackContext context)
	{
		if (context.phase == InputActionPhase.Started)
			interactEvent?.Invoke();
	}

	public void OnJump(InputAction.CallbackContext context)
	{
		if (context.phase == InputActionPhase.Started)
			jumpEvent?.Invoke();
	}

	public void OnMove(InputAction.CallbackContext context)
	{
		if (context.phase == InputActionPhase.Performed)
			moveEvent?.Invoke(context.ReadValue<Vector2>());
	}

	public void OnPause(InputAction.CallbackContext context)
	{
		if (context.phase == InputActionPhase.Started)
			pauseEvent?.Invoke();
	}
}
